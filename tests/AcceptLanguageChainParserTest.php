<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-accept-language-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\AcceptLanguageChain;
use PhpExtended\HttpClient\AcceptLanguageChainParser;
use PhpExtended\HttpClient\AcceptLanguageItem;
use PhpExtended\Locale\Locale;
use PHPUnit\Framework\TestCase;

/**
 * AcceptLanguageChainParserTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\AcceptLanguageChainParser
 * @internal
 * @small
 */
class AcceptLanguageChainParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AcceptLanguageChainParser
	 */
	protected AcceptLanguageChainParser $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$expected = new AcceptLanguageChain([
			new AcceptLanguageItem(new Locale('en_US')),
			new AcceptLanguageItem(new Locale('en'), 0.5),
		]);
		
		$this->assertEquals($expected, $this->_object->parse('en_US,en;q=0.5'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new AcceptLanguageChainParser();
	}
	
}
