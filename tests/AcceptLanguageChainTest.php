<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-accept-language-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\AcceptLanguageChain;
use PhpExtended\HttpClient\AcceptLanguageItem;
use PhpExtended\Locale\Locale;
use PHPUnit\Framework\TestCase;

/**
 * AcceptLanguageChainTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\AcceptLanguageChain
 * @internal
 * @small
 */
class AcceptLanguageChainTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AcceptLanguageChain
	 */
	protected AcceptLanguageChain $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testIteration() : void
	{
		foreach($this->_object as $key => $item)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(AcceptLanguageItem::class, $item);
		}
	}
	
	public function testCount() : void
	{
		$this->assertEquals(1, $this->_object->count());
	}
	
	public function testAppendItem() : void
	{
		$expected = new AcceptLanguageChain([
			new AcceptLanguageItem(new Locale('en_US'), 0.7),
			$appended = new AcceptLanguageItem(new Locale('fr_FR'), 0.5),
		]);
		
		$this->assertEquals($expected, $this->_object->appendItem($appended));
	}
	
	public function testPrependItem() : void
	{
		$expected = new AcceptLanguageChain([
			$prepended = new AcceptLanguageItem(new Locale('fr_FR'), 0.5),
			new AcceptLanguageItem(new Locale('en_US'), 0.7),
		]);
		
		$this->assertEquals($expected, $this->_object->prependItem($prepended));
	}
	
	public function testGetHeaderValue() : void
	{
		$this->assertEquals('en-US;q=0.7', $this->_object->getHeaderValue());
	}
	
	public function testIsEmpty() : void
	{
		$this->assertFalse($this->_object->isEmpty());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new AcceptLanguageChain([
			new AcceptLanguageItem(new Locale('en_US'), 0.7),
		]);
	}
	
}
