<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-accept-language-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\AcceptLanguageItem;
use PhpExtended\Locale\Locale;
use PHPUnit\Framework\TestCase;

/**
 * AcceptLanguageItemTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\AcceptLanguageItem
 * @internal
 * @small
 */
class AcceptLanguageItemTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AcceptLanguageItem
	 */
	protected AcceptLanguageItem $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetLocale() : void
	{
		$this->assertEquals(new Locale('en_US'), $this->_object->getLocale());
	}
	
	public function testGetQValue() : void
	{
		$this->assertEquals(0.7, $this->_object->getQValue());
	}
	
	public function testGetHeaderValue() : void
	{
		$this->assertEquals('en-US;q=0.7', $this->_object->getHeaderValue());
	}
	
	public function testGetHeaderValueDefault() : void
	{
		$this->assertEquals('en-US', (new AcceptLanguageItem(new Locale('en_US'), 1.0))->getHeaderValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new AcceptLanguageItem(new Locale('en_US'), 0.7);
	}
	
}
