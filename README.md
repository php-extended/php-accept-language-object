# php-extended/php-accept-language-object
A psr-18 compliant middleware client that handles accept-language headers.

![coverage](https://gitlab.com/php-extended/php-accept-language-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-accept-language-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-accept-language-object ^8`


## Basic Usage

This library is to make a man in the middle for http requests and responses
and logs the events when requests passes. It may be used the following way :

```php

/** @var $parser \PhpExtended\HttpClient\AcceptLaguageChainParser */
$parser = new AcceptLanguageChainParser();
$chain = $parser->parse('en-US,en;q=0.5');
// $chain is a AcceptLanguageChain with 2 AcceptLanguageItem

/** @var $request \Psr\Http\Message\RequestInterface */
$request->withHeader('Accept-Language', $chain->getHeaderValue());

```

This library handles the adding of `Accept-Language` headers
on the requests.


## License

MIT (See [license file](LICENSE)).
