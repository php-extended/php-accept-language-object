<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-accept-language-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Iterator;

/**
 * AcceptLanguageChain class file.
 * 
 * This class represents the chain of accept language items.
 * 
 * @author Anastaszor
 */
class AcceptLanguageChain implements AcceptLanguageChainInterface
{
	
	/**
	 * The items.
	 * 
	 * @var array<integer, AcceptLanguageItemInterface>
	 */
	protected array $_items = [];
	
	/**
	 * Builds a new AcceptLanguageChain with the given array of items.
	 * 
	 * @param array<integer, AcceptLanguageItemInterface> $items
	 */
	public function __construct(array $items)
	{
		$this->_items = $items;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_items);
	}
	
	/**
	 * {@inheritDoc}
	 * @see Iterator::current()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function current() : AcceptLanguageItemInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return \current($this->_items);
	}
	
	/**
	 * {@inheritDoc}
	 * @see Iterator::next()
	 */
	public function next() : void
	{
		\next($this->_items);
	}
	
	/**
	 * {@inheritDoc}
	 * @see Iterator::key()
	 */
	public function key() : int
	{
		return (int) \key($this->_items);
	}
	
	/**
	 * {@inheritDoc}
	 * @see Iterator::valid()
	 */
	public function valid() : bool
	{
		return \current($this->_items) !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see Iterator::rewind()
	 */
	public function rewind() : void
	{
		\reset($this->_items);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\HttpClient\AcceptLanguageChainInterface::appendItem()
	 */
	public function appendItem(AcceptLanguageItemInterface $item) : AcceptLanguageChainInterface
	{
		$this->_items[] = $item;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\HttpClient\AcceptLanguageChainInterface::prependItem()
	 */
	public function prependItem(AcceptLanguageItemInterface $item) : AcceptLanguageChainInterface
	{
		\array_unshift($this->_items, $item);
		
		return $this;
	}
	
	/**
	 * Gets the header value of the chain.
	 * 
	 * @return string
	 */
	public function getHeaderValue() : string
	{
		return \implode(',', \array_map(function(AcceptLanguageItemInterface $item)
		{
			return $item->getHeaderValue();
		}, $this->_items));
	}
	
	/**
	 * Gets whether this chain is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool
	{
		return 0 === \count($this->_items);
	}
	
}
