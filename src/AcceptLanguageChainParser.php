<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-accept-language-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use PhpExtended\Locale\Locale;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;

/**
 * AcceptLanguageChainParser class file.
 * 
 * This is a parser for Accept Language Chains.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\AbstractParser<AcceptLanguageChainInterface>
 */
class AcceptLanguageChainParser extends AbstractParser implements AcceptLanguageChainParserInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : object
	{
		$parts = \explode(',', (string) $data);
		$items = [];
		
		foreach($parts as $part)
		{
			$subparts = \explode(';', $part);

			try
			{
				$locale = new Locale($subparts[0]);
			}
			catch(InvalidArgumentException $exc)
			{
				throw new ParseException(Locale::class, $subparts[0], 0, 'Invalid locale selector.', -1, $exc);
			}

			$qvalue = 1.0;
			if(isset($subparts[1]))
			{
				$qvalue = (float) \str_replace('q=', '', $subparts[1]);
			}
			$items[] = new AcceptLanguageItem($locale, $qvalue);
		}
		
		return new AcceptLanguageChain($items);
	}
	
}
