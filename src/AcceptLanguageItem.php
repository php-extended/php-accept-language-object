<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-accept-language-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use PhpExtended\Locale\LocaleInterface;

/**
 * AcceptLanguageItem class file.
 * 
 * This class represents an item for the accept language chain.
 * 
 * @author Anastaszor
 */
class AcceptLanguageItem implements AcceptLanguageItemInterface
{
	
	/**
	 * The locale.
	 * 
	 * @var LocaleInterface
	 */
	protected LocaleInterface $_locale;
	
	/**
	 * The q-value.
	 * 
	 * @var float
	 */
	protected float $_qvalue = 1.0;
	
	/**
	 * Builds a new AcceptLanguageItem with the given locale and q-value.
	 * 
	 * @param LocaleInterface $locale
	 * @param float $qvalue
	 */
	public function __construct(LocaleInterface $locale, float $qvalue = 1.0)
	{
		$this->_locale = $locale;
		$this->_qvalue = \round(\min(1.0, \max(0.0, $qvalue)), 2);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the locale.
	 * 
	 * @return LocaleInterface
	 */
	public function getLocale() : LocaleInterface
	{
		return $this->_locale;
	}
	
	/**
	 * The q-value of this item.
	 * 
	 * @return float
	 */
	public function getQValue() : float
	{
		return $this->_qvalue;
	}
	
	/**
	 * Gets the header value of this accept language item.
	 * 
	 * @return string
	 */
	public function getHeaderValue() : string
	{
		if(1.0 === $this->getQValue())
		{
			return $this->getLocale()->toAcceptHttpHeader();
		}
		
		return $this->getLocale()->toAcceptHttpHeader().';q='.((string) $this->getQValue());
	}
	
}
